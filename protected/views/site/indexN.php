<?php
$this->pageTitle = Yii::app()->name;
?>

<div class="col-sm-8">
    <h1 class="light text-white">Estamos actualizando la pagina web</h1>
    <h4 class="text-white">Muy pronto nuevas novedades</h4>
    <?php
    if (isset($mail['estado'])) {
        if ($mail['estado']) {
            echo '<div class="alert alert-success">Ya estas suscrito :), Gracias!</div>';
        } else {
            echo '<div class="alert alert-danger">Ocurrio un error, intente nuevamente</div>';
        }
    }
    ?>
    <form class="m-t-25 m-b-20" method="POST">
        <div class="form-group form-group-default input-group no-border input-group-attached col-md-12  col-sm-12 col-xs-12">
            <label>Correo Electronico</label>
            <input type="email" class="form-control" name="correo" placeholder="correo@correo.com">
            <span class="input-group-btn">
                <button class="btn btn-black btn-cons" type="submit">Subscribete !!</button>
            </span>
        </div>
    </form>
    <p class="text-white fs-12">Te avisaremos cuando tengamos novedades para ti!.</p>
</div>
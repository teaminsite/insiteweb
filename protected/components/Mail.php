<?php

class Mail {

    /**
     * Recibe en el mismo orden los siguientes parametros
     * @param string $para email de destino
     * @param string $mensaje HTML , esto tambien permite recibir plantillas 
     * @param string $asunto asunto del mensaje
     * @param string $de Email del emisor
     * Importante a tener encuenta que esta funcion depende de los parametros
     * brindados en el archivo ubicado config/main.php
     * @return array() $data['estado'] true si se envío con éxito, false si hay error
     * @return array() $data['mensaje'] Descripcion del mensaje
     */
    public static function SendMail($para = "raliaga@insite.pe", $mensaje = false, $asunto = "contacto Insite") {
        if ($mensaje == false) {
            $mensaje = Constantes::msgTemplate;
        }
        $mail = Yii::app()->Smtpmail;
        $mail->ClearAllRecipients();
        $mail->IsHTML(true);
        $mail->isSMTP();
        $mail->SetFrom("contacto@insite.pe", "Contacto InSite");
        $mail->MsgHTML($mensaje);

        $mail->Subject = $asunto;
        if (is_array($para)) {
            foreach ($para as $destinatario) {
                $mail->AddAddress($destinatario, "");
            }
        } else {
            $mail->AddAddress($para, "");
        }
        $mail->Priority = 3;
        if (!$mail->Send()) {
            $data['estado']  = false;
            $data['mensaje'] = "Mailer Error: " . $mail->ErrorInfo;
        } else {
            $data['estado']  = true;
            $data['mensaje'] = "Mensaje Enviado!";
        }
        return $data;
    }

}

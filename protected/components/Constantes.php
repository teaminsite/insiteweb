<?php

class Constantes {

    Const msgTemplate = '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family:Calibri">
    <tr>
        <td align="center" valign="top">
            <table border="0" cellpadding="5" cellspacing="0" width="600">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="5" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top">
                                    <img src="https://s14.postimg.org/bjjdaomxt/header.png" alt="" width="100%" height="180" style="display: block;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <hr>
                        <table border="0" cellpadding="5" cellspacing="0" width="100%">
                            <tr>
                                <td align="" valign="top" width="60%">
                                    <h3 style="font-weight: bolder; text-align: center">Gracias por suscribirte</h3>
                                    <p align="justify">Ahora nos pondremos en contacto cada vez que tengamos una novedad que te podr&iacute;a interesar!</p>
                                    <p align="justify">Pero pierde cuidado no te haremos SPAM, tampoco venderemos tus datos, tu privacidad es tan importa como la nuestra propia.</p>
                                    <p>Hasta la proxima, Saludos!</p>
                                    <p><strong><a href="http://www.insite.pe" target="_blank" style="text-decoration: none">Grupo InSite</a></strong></p>
                                </td>
                                <td align="center" valign="top"  width="220" height="200">
                                    <a href="http://https://www.youtube.com/embed/PZXaQijiiAA" target="_blank"><img src="https://s22.postimg.org/68e26spxd/roberto.gif"/></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <hr>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td valign="top">
                                    <small style="font-size: 11px">
                                        Si!, eso nada mas. Siempre trataremos de mantener las comunicaciones simples y  directas
                                    </small>
                                    <small style="font-weight: bolder;float:right; font-size: 11px">
                                        Grupo InSite © 2016
                                    </small>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>';

}

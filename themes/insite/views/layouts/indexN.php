<!DOCTYPE html>
<html>
    <?php $this->renderPartial('//layouts/pages/head') ?>
    <body id="page-top" class="index">

        <!-- Navigation -->
        <?php $this->renderPartial('//layouts/sections/menu') ?>

        <!-- Header -->
        <?php $this->renderPartial('//layouts/sections/header') ?>

        <!-- Services Section -->
        <?php $this->renderPartial('//layouts/sections/servicios') ?>

        <!-- About Section -->
        <?php $this->renderPartial('//layouts/sections/about') ?>

        <!-- Nosotros Section -->
        <?php $this->renderPartial('//layouts/sections/nosotros') ?>


        <!-- Contact Section -->
        <?php $this->renderPartial('//layouts/sections/contacto') ?>


        <?php $this->renderPartial('//layouts/sections/footer') ?>

        <script type="text/javascript" src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/jquery-1.11.0.min.js"></script>
        <script src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/classie.js"></script>
        <script src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/cbpAnimatedHeader.js"></script>
        <script src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/agency.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

</html>
<header>
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">Bienvenidos</div>
            <div class="intro-heading">Grupo Insite</div>
            <a href="#services" class="page-scroll btn btn-xl">Conoce más</a>
        </div>
    </div>
</header>
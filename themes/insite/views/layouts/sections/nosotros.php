<section id="team" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Nosotros</h2>
                <h3 class="section-subheading text-muted">Nuestro Equipo, Siempre listo.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="<?= Yii::app()->theme->baseUrl; ?>/bin/img/renzo.jpg" class="img-responsive img-circle" alt="">
                    <h4>Renzo Aliaga</h4>
                    <p class="text-muted">Lider de Proyectos</p>
<!--                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>-->
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="<?= Yii::app()->theme->baseUrl; ?>/bin/img/elias.jpg" class="img-responsive img-circle" alt="">
                    <h4>Eli Fuentes</h4>
                    <p class="text-muted">HelpDesk / Desarrollador</p>
<!--                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>-->
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="<?= Yii::app()->theme->baseUrl; ?>/bin/img/nolberto.jpg" class="img-responsive img-circle" alt="">
                    <h4>Nolberto Vilchez</h4>
                    <p class="text-muted">Lider Desarrolador</p>
<!--                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <!--<p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>-->
            </div>
        </div>
    </div>
</section>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">InSite</h2>
                <h3 class="section-subheading text-muted">El Grupo InSite abriendo camino a la innovación</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="timeline">
                    <li>
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="<?= Yii::app()->theme->baseUrl; ?>/bin/img/timeline-01.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2011-2015</h4>
                                <h4 class="subheading">Pre - InSite</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">
                                    Durante este lapso, los socios de InSite, aun no sabian que iban a trabajar juntos,
                                    pero cada uno por su parte ya hacia sus intentos de innovar en la tecnologia por 
                                    pequeños proyectos.
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="<?= Yii::app()->theme->baseUrl; ?>/bin/img/timeline-02.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>Setiembre 2015</h4>
                                <h4 class="subheading">Inception</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">
                                    Renzo Aliaga y Nolberto Vilchez ya trabajando juntos en algunos y realizando algunos freelance,
                                    deciden formalizar estos trabajos y fusionar esfuerzos para bien.
                                </p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="<?= Yii::app()->theme->baseUrl; ?>/bin/img/timeline-03.jpg" alt="">">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>Enero 2016</h4>
                                <h4 class="subheading">El Tercer Mosquetero</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">
                                    Elí Fuentes se une al equipo, cuando InSite aun era solo una idea, 
                                    el por su parte ya trabaja con datos de SUNAT y asi fue como el equipo se reunio.
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="<?= Yii::app()->theme->baseUrl; ?>/bin/img/logo.png" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>Mayo 2016</h4>
                                <h4 class="subheading">InSite nace</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">
                                    Después de decidirnos en hacer formal el trabajo, oficialmente el Grupo InSite nace el 23 de Mayo del 2016                                   
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>Se parte
                                <br>de esta
                                <br>historia.</h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
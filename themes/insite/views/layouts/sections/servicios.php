<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Servicios</h2>
                <h3 class="section-subheading text-muted">Disponemos de acceso a la información que necesitas cuando y como lo necesitas</h3>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-money fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Economico</h4>
                <p class="text-muted">
                    Paga solo lo que gastas con nuestras tarifas super bajas. Además disponemos de bolsas de SMS y bolsas de consultas
                    sin plazo de termino, ni letras chiquitas, solo pagas lo que consumes, sin ataduras ni contratos engorrosos.
                    El momento del pago se te otorga un token por cada servicio para que tengas en orden lo consumido y en que momento debes
                    recargar tu bolsa de consumo.
                </p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-database fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Data a consumir</h4>
                <p class="text-muted">
                    Disponemos de informacion de la SUNAT, por contribuyente, actualizado al DIA. Tenemos muchos datos que puedes consumir
                    mediante WebServices, directo para que lo utilices en tus aplicaciones o puedes consultar directamente desde la consola
                    web de usuario.
                </p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">SMS</h4>
                <p class="text-muted">
                    Disponemos de bolsas de SMS para que ademas de comunicarte con tus usuarios o clientes mediante correo electronico, les 
                    envies una alerta directamente a su celular, de esta manera no se perdera la comunicacion entre tu y tus usuarios.
                    Usalo para lo que mejor te convenga, desde la plataforma de SMS InSite o integralo en tu sistema o aplicacion web.
                </p>
            </div>
        </div>
    </div>
</section>
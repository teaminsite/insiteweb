<!DOCTYPE html>
<html>
    <?php $this->renderPartial('//layouts/pages/head') ?>
    <style>
        @media (min-width: 768px){
            section {
                padding: 0px !important; 
            }
        }
        section {
            padding:  0px !important;
        }
    </style>
    <body class="pace-white">
        <!-- BEGIN JUMBOTRON -->
        <section class="jumbotron demo-custom-height xs-full-height bg-black" data-pages-bg-image="<?= Yii::app()->theme->baseUrl; ?>/bin/img/back_01.jpg">
            <div class="container-xs-height full-height">
                <div class="col-xs-height col-middle text-left">
                    <div class="container">
                        <?= $content ?>

                    </div>
                </div>
            </div>
        </section>
        <!-- END JUMBOTRON -->
        <!-- START FOOTER -->
        <section class="p-t-10">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <!--<img src="<?php // Yii::app()->theme->baseUrl;   ?>/bin/img/back_01.jpg" width="152" height="21" data-src-retina="<?= Yii::app()->theme->baseUrl; ?>/bin/img/back_01.jpg" class="logo inline m-r-50" alt="">-->
                        <!--                        <div class="m-t-10 ">
                                                    <ul class="no-style fs-11 no-padding font-arial">
                                                        <li class="inline no-padding"><a href="#" class=" text-master p-r-10 b-r b-grey">Home</a></li>
                                                        <li class="inline no-padding"><a href="#" class="hint-text text-master p-l-10 p-r-10 b-r b-grey">Themeforest Profile</a></li>
                                                        <li class="inline no-padding"><a href="#" class="hint-text text-master p-l-10 p-r-10 b-r b-grey">Support</a></li>
                                                        <li class="inline no-padding"><a href="#" class="hint-text text-master p-l-10 p-r-10 xs-no-padding xs-m-t-10">Made with Pages</a></li>
                                                    </ul>
                                                </div>-->
                    </div>
                    <div class="col-sm-6 text-right font-arial sm-text-left">
<!--                        <p class="fs-11 no-margin small-text">
                            <span class="hint-text">Exclusive only at</span> 
                            Envato Marketplace,Themeforest 
                            <span class="hint-text">See</span> 
                            Standard licenses &amp; Extended licenses
                        </p>-->
                        <p class="fs-11">
                            <img src="<?= Yii::app()->theme->baseUrl; ?>/bin/img/logo.png" height="18px" /> Copyright &copy; 2016 InSite  Todos los derechos reservados.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- END FOOTER -->
        <!-- BEGIN CORE FRAMEWORK -->
        <!-- Template js -->

        <!-- BEGIN CORE FRAMEWORK -->
        <script type="text/javascript" src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/jquery-1.11.0.min.js"></script>
        <script src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/pace/pace.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/pages/js/pages.image.loader.js"></script>
        <!-- BEGIN RETINA IMAGE LOADER -->
        <script type="text/javascript" src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/jquery-unveil/jquery.unveil.min.js"></script>
        <!-- END VENDOR JS -->
        <!-- BEGIN PAGES FRONTEND LIB -->
        <script type="text/javascript" src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/pages/js/pages.frontend.js"></script>
        <!-- END PAGES LIB -->
</html>
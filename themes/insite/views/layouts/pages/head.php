<?php $action = Yii::app()->controller->action->id; ?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>InSite :: Group</title>
        <!-- Custom CSS -->
        <link href="<?= Yii::app()->theme->baseUrl; ?>/bin/css/font-awesome.css" rel="stylesheet">
        <link href="<?= Yii::app()->theme->baseUrl; ?>/bin/css/ethereal.css" rel="stylesheet">
        <noscript><link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl; ?>/bin/css/noscript.css" /></noscript>

        <script src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/jquery-1.11.0.min.js"></script>
        <script src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/ethereal/skel.min.js"></script>
        <script src="<?= Yii::app()->theme->baseUrl; ?>/bin/js/ethereal/main.js"></script>
    </head>
    <body>

<header id="header_wrapper" >
    <div class="container">
        <div class="header_box">
            <div class="logo"><a href="<?= Yii::app()->baseUrl; ?>"><img src="<?= Yii::app()->theme->baseUrl; ?>/bin/img/logo.png" alt="logo"></a></div>
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav"> 
                        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> </button>
                </div>
                <div id="main-nav" class="collapse navbar-collapse navStyle">
                    <ul class="nav navbar-nav" id="mainNav">
                        <li class="<?php if ($action=='login' ||$action=='regusu'){ echo '';}else{ echo 'active';}?>">
                            <a href="<?= Yii::app()->controller->createUrl("{$this->id}/index") ?>#hero_section" class="scroll-link">Inicio</a>
                        </li>
                        <li>
                            <a href="<?= Yii::app()->controller->createUrl("{$this->id}/index") ?>#aboutUs" class="scroll-link">Nosotros</a>
                        </li>
                        <li>
                            <a href="<?= Yii::app()->controller->createUrl("{$this->id}/index") ?>#service" class="scroll-link">Servicios</a>
                        </li>
                        <!--<li><a href="#Portfolio" class="scroll-link">Portafolio</a></li>-->
                        <li>
                            <a href="<?= Yii::app()->controller->createUrl("{$this->id}/index") ?>#clients" class="scroll-link">Tecnolog&iacute;a</a>
                        </li>
                        <!--<li><a href="#team" class="scroll-link">Equipo</a></li>-->
                        <li>
                            <a href="<?= Yii::app()->controller->createUrl("{$this->id}/index") ?>#contact" class="scroll-link">Contacto</a>
                        </li>
                        <li class="<?php if ($action=='login' ||$action=='regusu'){ echo 'active';}else{ echo '';}?>">
                            <a href="<?= Yii::app()->controller->createUrl("{$this->id}/login") ?>" class="scroll-link">Acceso</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>